﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace rectangle
{
	[Activity (Label = "rectangle", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		#region degiskenler
		public ImageView img1;
		public ImageView img2;
		public ImageView img3;
		public ImageView img4;
		public ImageView img5;
		public ImageView img6;
		public ImageView img7;
		public ImageView img8;
		public ImageView img9;
		public ImageView img10;
		public ImageView img11;
		public ImageView img12;
		public ImageView img13;
		public ImageView img14;
		public ImageView img15;
		public ImageView img16;
		public EditText Kullanici;
		public TextView moveText;
		public Button OK;
		public Button button;
		public Button RightBtn;
		public Button UpBtn;
		public Button DownBtn;
		public Button info;
		public Button sifirla;
		public Button basla;
		int[] m2 = { 0, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2 };
		int[] m3 = { 0, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2 };
		string move = "";
		string[] way = { "L", "R", "U", "D" };
		string[] chr = {};
		int sayac = 0;
		int a = 0;
		#endregion
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);

			this.Title = "Puzzle";

			var textBox = FindViewById<TextView> (Resource.Id.tb);
			img1 = FindViewById<ImageView> (Resource.Id.box1);
			img2 = FindViewById<ImageView> (Resource.Id.box2);
			img3 = FindViewById<ImageView> (Resource.Id.box3);
			img4 = FindViewById<ImageView> (Resource.Id.box4);
			img5 = FindViewById<ImageView> (Resource.Id.box5);
			img6 = FindViewById<ImageView> (Resource.Id.box6);
			img7 = FindViewById<ImageView> (Resource.Id.box7);
			img8 = FindViewById<ImageView> (Resource.Id.box8);
			img9 = FindViewById<ImageView> (Resource.Id.box9);
			img10 = FindViewById<ImageView> (Resource.Id.box10);
			img11 = FindViewById<ImageView> (Resource.Id.box11);
			img12 = FindViewById<ImageView> (Resource.Id.box12);
			img13 = FindViewById<ImageView> (Resource.Id.box13);
			img14 = FindViewById<ImageView> (Resource.Id.box14);
			img15 = FindViewById<ImageView> (Resource.Id.box15);
			img16 = FindViewById<ImageView> (Resource.Id.box16);
			Kullanici = FindViewById<EditText> (Resource.Id.tBox);
			moveText = FindViewById<TextView> (Resource.Id.moveTxt);
			button = FindViewById<Button> (Resource.Id.btn);
			RightBtn = FindViewById<Button> (Resource.Id.btn3);
			DownBtn = FindViewById<Button> (Resource.Id.btn2);
			UpBtn = FindViewById<Button> (Resource.Id.btn4);
			OK = FindViewById<Button> (Resource.Id.userBtn);
			info = FindViewById<Button> (Resource.Id.infoBtn);
			sifirla = FindViewById<Button> (Resource.Id.sifir);
			basla = FindViewById<Button> (Resource.Id.baslaBtn);

			AlertDialog.Builder builder = new AlertDialog.Builder (this);
			builder.SetTitle ("Kullanım Kılavuzu");
			builder.SetMessage("Bu bir Fifteen Puzzle projesidir. " +
				"Kutuların hemen altında bulunan Left, Right, Up ve Down buttonları ile kutuların kontrollerini sağlayabilirsiniz. " +
				"Ayrıca bu buttonların altında bulunan textbox'a dilediğiniz kombinasyonu (llrud,LRUDLL,LuuUdR) girerek kutuların son " +
				"haline ulaşabilirsiniz. Bir diğer özellik olan; rastgele hareket için START button'una basabilir, arka planda oluşturulan " +
				"kombinasyonun kutular üzerindeki yarattığı düzeni görebilirsiniz. Gökhan BOZKUŞ\n");


			info.Click += (object sender, EventArgs e) => 
			{
				AlertDialog dialog = builder.Create();
				dialog.Show();

			};
					


			sifirla.Click += (object sender, EventArgs e) => 
			{
				first();
				//diziSifirla();
				//m2 = m3;
				//int[] m2 = { 0, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2, 1, 1, 2, 2 };
				//for (int x=0; x<m3.Length; x++)
				//{
				//	int y = m3[x];
				//	m2[x]=y;
				//}
				//sayac = 0;
				//a = 0;
			};


			//Kullanıcının bir hareket kombinasyonu girmesiyle tetiklenecek işlemler
			//Kullanıcı L R U D değerlerinden başka karakter veya boşluk girmesi durumunda if yapısı sayesinde hata alınmayacak
			OK.Click += (object sender, EventArgs e) => 
			{
				first();

				if (Kullanici.Text != string.Empty)
				{
					try
					{
						textBox.Text = Kullanici.Text.ToUpper();
					}
					catch{}
				

				char[] karakterler = textBox.Text.ToCharArray();
				//textBox.Text = karakterler[0].ToString();
				for (int i=0; i<=karakterler.Length; i++)
				{
					try
					{
					if (karakterler[i].ToString() == "L")
						button.PerformClick();
						
					else if (karakterler[i].ToString() == "R")
						RightBtn.PerformClick();
					else if (karakterler[i].ToString() == "U")
						UpBtn.PerformClick();
					else if (karakterler[i].ToString() == "D")
						DownBtn.PerformClick();
					}
					catch{}
				}
				}
			};


			//Rastgele hareket işlemi
			basla.Click += (object sender, EventArgs e) => 
			{
				first();
				for (int y=0; y<17; y++)
				{
					Random random = new Random();
					int rnd = random.Next(0,4);
					//moveText.Text = rnd.ToString();
					switch(rnd){
					case 0:
						button.PerformClick();
						break;
					case 1:
						RightBtn.PerformClick();
						break;
					case 2:
						UpBtn.PerformClick();
						break;
					case 3: 
						DownBtn.PerformClick();
						break;
					}
						

				}
			};


			//Left işlemi 
			//Switch case yapısı sayesinde hareket alanı sınırlandırıldı, eğer boşluk en sağda ise bir alta geçilmesi engellendi
			button.Click += (object sender, EventArgs e) => {
				switch (sayac) {
				case 3:
					break;
				case 7:
					break;
				case 11:
					break;
				case 15:
					break;
				default:
					//Eğer hareket gerçekleştiyse hareket listesine eklesin
					move = move + "L";
					moveText.Text = move.ToString();
					a = m2 [sayac];
					m2 [sayac] = m2 [sayac + 1];
					m2 [sayac + 1] = a;
					sayac++;
					a = 0;
					//textBox.Text = m2 [0].ToString () + m2 [1].ToString () + m2 [2].ToString () + m2 [3].ToString () + "+" + m2 [4].ToString () + m2 [5].ToString () + m2 [6].ToString () + m2 [7].ToString ();
				//textBox.Text = kutuList[0].ToString();
					BoxCtrl1 ();
					BoxCtrl2 ();
					BoxCtrl3 ();
					BoxCtrl4 ();
					BoxCtrl5 ();
					BoxCtrl6 ();
					BoxCtrl7 ();
					BoxCtrl8 ();
					BoxCtrl9 ();
					BoxCtrl10 ();
					BoxCtrl11 ();
					BoxCtrl12 ();
					BoxCtrl13 ();
					BoxCtrl14 ();
					BoxCtrl15 ();
					BoxCtrl16 ();
					break;
				}
			};

			//Up işlemi
			//Switch case yapısı sayesinde hareket alanı sınırlandırıldı, eğer boşluk en aşağıda ise en üste geçilmesi engellendi
			UpBtn.Click += (object sender, EventArgs e) => {
				switch (sayac) {
				case 12:
					break;
				case 13:
					break;
				case 14:
					break;
				case 15:
					break;
				default:
					//Eğer hareket gerçekleştiyse hareket listesine eklesin
					move = move + "U";
					moveText.Text = move.ToString();
					a = m2 [sayac];
					m2 [sayac] = m2 [sayac + 4];
					m2 [sayac + 4] = a;
					sayac = sayac + 4;
					a = 0;
					//textBox.Text = m2 [0].ToString () + m2 [1].ToString () + m2 [2].ToString () + m2 [3].ToString () + "+" + m2 [4].ToString () + m2 [5].ToString () + m2 [6].ToString () + m2 [7].ToString ();

					BoxCtrl1 ();
					BoxCtrl2 ();
					BoxCtrl3 ();
					BoxCtrl4 ();
					BoxCtrl5 ();
					BoxCtrl6 ();
					BoxCtrl7 ();
					BoxCtrl8 ();
					BoxCtrl9 ();
					BoxCtrl10 ();
					BoxCtrl11 ();
					BoxCtrl12 ();
					BoxCtrl13 ();
					BoxCtrl14 ();
					BoxCtrl15 ();
					BoxCtrl16 ();
						break;
				}
			};

			//Right işlemi
			//Switch case yapısı sayesinde hareket alanı sınırlandırıldı, eğer boşluk en solda ise bir üste geçilmesi engellendi
			RightBtn.Click += (object sender, EventArgs e) => {
				switch (sayac) {
				case 0:
					break;
				case 4:
					break;
				case 8:
					break;
				case 12:
					break;
				default:
					//Eğer hareket gerçekleştiyse hareket listesine eklesin
					move = move + "R";
					moveText.Text = move.ToString();
					a = m2 [sayac];
					m2 [sayac] = m2 [sayac - 1];
					m2 [sayac - 1] = a;
					sayac--;
					a = 0;
					//textBox.Text = m2 [0].ToString () + m2 [1].ToString () + m2 [2].ToString () + m2 [3].ToString () + "+" + m2 [4].ToString () + m2 [5].ToString () + m2 [6].ToString () + m2 [7].ToString ();

					BoxCtrl1 ();
					BoxCtrl2 ();
					BoxCtrl3 ();
					BoxCtrl4 ();
					BoxCtrl5 ();
					BoxCtrl6 ();
					BoxCtrl7 ();
					BoxCtrl8 ();
					BoxCtrl9 ();
					BoxCtrl10 ();
					BoxCtrl11 ();
					BoxCtrl12 ();
					BoxCtrl13 ();
					BoxCtrl14 ();
					BoxCtrl15 ();
					BoxCtrl16 ();
					break;
				}
				
			};

			//Down işlemi
			//Switch case yapısı sayesinde hareket alanı sınırlandırıldı, eğer boşluk en yukarıda ise en alta geçilmesi engellendi
			DownBtn.Click += (object sender, EventArgs e) => {
				switch (sayac) {
				case 0:
					break;
				case 1:
					break;
				case 2:
					break;
				case 3:
					break;
				default:
					//Eğer hareket gerçekleştiyse hareket listesine eklesin
					move = move + "D";
					moveText.Text = move.ToString();
					a = m2 [sayac];
					m2 [sayac] = m2 [sayac - 4];
					m2 [sayac - 4] = a;
					sayac = sayac - 4;
					a = 0;
					//textBox.Text = m2 [0].ToString () + m2 [1].ToString () + m2 [2].ToString () + m2 [3].ToString () + "+" + m2 [4].ToString () + m2 [5].ToString () + m2 [6].ToString () + m2 [7].ToString ();

					BoxCtrl1 ();
					BoxCtrl2 ();
					BoxCtrl3 ();
					BoxCtrl4 ();
					BoxCtrl5 ();
					BoxCtrl6 ();
					BoxCtrl7 ();
					BoxCtrl8 ();
					BoxCtrl9 ();
					BoxCtrl10 ();
					BoxCtrl11 ();
					BoxCtrl12 ();
					BoxCtrl13 ();
					BoxCtrl14 ();
					BoxCtrl15 ();
					BoxCtrl16 ();
						break;
				}
				
			};
		}

		//Kutuların ilk hali
		public void first()
		{
			moveText.Text = "";
			move ="";
			img1.SetBackgroundColor(Color.White);
			img2.SetBackgroundColor(Color.Red);
			img3.SetBackgroundColor(Color.Blue);
			img4.SetBackgroundColor(Color.Blue);
			img5.SetBackgroundColor(Color.Red);
			img6.SetBackgroundColor(Color.Red);
			img7.SetBackgroundColor(Color.Blue);
			img8.SetBackgroundColor(Color.Blue);
			img9.SetBackgroundColor(Color.Red);
			img10.SetBackgroundColor(Color.Red);
			img11.SetBackgroundColor(Color.Blue);
			img12.SetBackgroundColor(Color.Blue);
			img13.SetBackgroundColor(Color.Red);
			img14.SetBackgroundColor(Color.Red);
			img15.SetBackgroundColor(Color.Blue);
			img16.SetBackgroundColor(Color.Blue);
		

			for (int x=0; x<m3.Length; x++)
			{
				int y = m3[x];
				m2[x]=y;
			}
			sayac = 0;
			a = 0;
		}

			
		//Kutular için renk değiştirme fonksiyonları
		public void BoxCtrl1()
		{
			if (m2[0] == 1)
			{
				img1.SetBackgroundColor(Color.Red);
			}
			else if (m2[0] == 0)
			{
				img1.SetBackgroundColor(Color.White);
			}
			else if (m2[0]==2)
			{
				img1.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl2()
		{
			if (m2[1] == 1)
			{
				img2.SetBackgroundColor(Color.Red);
			}
			else if (m2[1] == 0)
			{
				img2.SetBackgroundColor(Color.White);
			}
			else if (m2[1]==2)
			{
				img2.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl3()
		{
			if (m2[2] == 1)
			{
				img3.SetBackgroundColor(Color.Red);
			}
			else if (m2[2] == 0)
			{
				img3.SetBackgroundColor(Color.White);
			}
			else if (m2[2]==2)
			{
				img3.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl4()
		{
			if (m2[3] == 1)
			{
				img4.SetBackgroundColor(Color.Red);
			}
			else if (m2[3] == 0)
			{
				img4.SetBackgroundColor(Color.White);
			}
			else if (m2[3]==2)
			{
				img4.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl5()
		{
			if (m2[4] == 1)
			{
				img5.SetBackgroundColor(Color.Red);
			}
			else if (m2[4] == 0)
			{
				img5.SetBackgroundColor(Color.White);
			}
			else if (m2[4]==2)
			{
				img5.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl6()
		{
			if (m2[5] == 1)
			{
				img6.SetBackgroundColor(Color.Red);
			}
			else if (m2[5] == 0)
			{
				img6.SetBackgroundColor(Color.White);
			}
			else if (m2[5]==2)
			{
				img6.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl7()
		{
			if (m2[6] == 1)
			{
				img7.SetBackgroundColor(Color.Red);
			}
			else if (m2[6] == 0)
			{
				img7.SetBackgroundColor(Color.White);
			}
			else if (m2[6]==2)
			{
				img7.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl8()
		{
			if (m2[7] == 1)
			{
				img8.SetBackgroundColor(Color.Red);
			}
			else if (m2[7] == 0)
			{
				img8.SetBackgroundColor(Color.White);
			}
			else if (m2[7]==2)
			{
				img8.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl9()
		{
			if (m2[8] == 1)
			{
				img9.SetBackgroundColor(Color.Red);
			}
			else if (m2[8] == 0)
			{
				img9.SetBackgroundColor(Color.White);
			}
			else if (m2[8]==2)
			{
				img9.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl10()
		{
			if (m2[9] == 1)
			{
				img10.SetBackgroundColor(Color.Red);
			}
			else if (m2[9] == 0)
			{
				img10.SetBackgroundColor(Color.White);
			}
			else if (m2[9]==2)
			{
				img10.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl11()
		{
			if (m2[10] == 1)
			{
				img11.SetBackgroundColor(Color.Red);
			}
			else if (m2[10] == 0)
			{
				img11.SetBackgroundColor(Color.White);
			}
			else if (m2[10]==2)
			{
				img11.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl12()
		{
			if (m2[11] == 1)
			{
				img12.SetBackgroundColor(Color.Red);
			}
			else if (m2[11] == 0)
			{
				img12.SetBackgroundColor(Color.White);
			}
			else if (m2[11]==2)
			{
				img12.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl13()
		{
			if (m2[12] == 1)
			{
				img13.SetBackgroundColor(Color.Red);
			}
			else if (m2[12] == 0)
			{
				img13.SetBackgroundColor(Color.White);
			}
			else if (m2[12]==2)
			{
				img13.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl14()
		{
			if (m2[13] == 1)
			{
				img14.SetBackgroundColor(Color.Red);
			}
			else if (m2[13] == 0)
			{
				img14.SetBackgroundColor(Color.White);
			}
			else if (m2[13]==2)
			{
				img14.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl15()
		{
			if (m2[14] == 1)
			{
				img15.SetBackgroundColor(Color.Red);
			}
			else if (m2[14] == 0)
			{
				img15.SetBackgroundColor(Color.White);
			}
			else if (m2[14]==2)
			{
				img15.SetBackgroundColor(Color.Blue);
			}
		}
		public void BoxCtrl16()
		{
			if (m2[15] == 1)
			{
				img16.SetBackgroundColor(Color.Red);
			}
			else if (m2[15] == 0)
			{
				img16.SetBackgroundColor(Color.White);
			}
			else if (m2[15]==2)
			{
				img16.SetBackgroundColor(Color.Blue);
			}
		}

	}
}